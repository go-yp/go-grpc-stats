up:
	sudo docker-compose up -d

down:
	sudo docker-compose down

yaaws-database-create:
	sudo docker exec yaaws_clickhouse clickhouse-client --query "CREATE DATABASE yaaws;"

test-golang-container:
	sudo docker exec yaaws_app go run main.go

benchmark-golang-container:
	# https://stackoverflow.com/questions/59471545/when-trying-to-build-docker-image-i-get-gcc-executable-file-not-found-in-p
	# sudo docker exec yaaws_app apk add build-base
	sudo docker exec yaaws_app go test ./examples/001-clickhouse-user-online-statistics-benchmark/... -v -bench=. -benchmem

benchmark-golang-container-100s:
	sudo docker exec yaaws_app go test ./examples/001-clickhouse-user-online-statistics-benchmark/... -v -bench=. -benchmem -benchtime=100s

test-clickhouse-container:
	sudo docker exec yaaws_clickhouse clickhouse-client --query "SELECT VERSION();"

test-clickhouse-user_online_source_statistics-count:
	sudo docker exec yaaws_clickhouse clickhouse-client --query "SELECT COUNT() FROM yaaws.user_online_source_statistics;"

proto-install:
	$(eval PROTOC_VERSION := 3.17.3)
	$(eval PROTOC_ZIP := protoc-$(PROTOC_VERSION)-linux-x86_64.zip)
	curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v$(PROTOC_VERSION)/$(PROTOC_ZIP)
	sudo unzip -o $(PROTOC_ZIP) -d /usr/local bin/protoc
	sudo unzip -o $(PROTOC_ZIP) -d /usr/local 'include/*'
	rm -f $(PROTOC_ZIP)
	sudo chmod 777 /usr/local/bin/protoc
	protoc --version
	GO111MODULE=on go get -u github.com/golang/protobuf/proto@v1.3.4
	GO111MODULE=on go get github.com/golang/protobuf/protoc-gen-go@v1.3.4

proto:
	protoc -I . protos/services/stats/*.proto --go_out=plugins=grpc:models