package main

import (
	"database/sql"
	"fmt"
	"log"
)

const (
	// language=SQL
	insertSQL = `INSERT INTO user_online_source_statistics (date, time, user_id, role)
VALUES (?, ?, ?, ?);`
)

func StoreUserOnlineStatistics(connect *sql.DB, users []UserOnlineEvent) (err error) {
	return execTx(connect, func(tx *sql.Tx) error {
		stmt, stmtErr := tx.Prepare(insertSQL)
		if stmtErr != nil {
			return stmtErr
		}
		defer stmt.Close()

		for _, user := range users {
			_, insertErr := stmt.Exec(
				user.Time,
				user.Time,
				user.UserID,
				user.Role,
			)

			if insertErr != nil {
				log.Printf("insert err %+v", insertErr)

				continue
			}
		}

		return nil
	})
}

func execTx(connect *sql.DB, fn func(*sql.Tx) error) error {
	var tx, txErr = connect.Begin()
	if txErr != nil {
		return txErr
	}

	var err = fn(tx)

	if err != nil {
		if rbErr := tx.Rollback(); rbErr != nil {
			return fmt.Errorf("tx err: %v, rb err: %v", err, rbErr)
		}
		return err
	}

	return tx.Commit()
}
