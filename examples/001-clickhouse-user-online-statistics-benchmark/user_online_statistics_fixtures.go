package main

import "time"

func userOnlineEventFixtures(count int) []UserOnlineEvent {
	var result = make([]UserOnlineEvent, count)

	var now = int32(time.Now().Unix())

	for i := 0; i < count; i++ {
		result[i] = UserOnlineEvent{
			Time:   now,
			UserID: uint32(i),
			Role:   role(i),
		}
	}

	return result
}

func role(index int) uint8 {
	const (
		employee = 1
		employer = 2
	)

	if index&1 == 1 {
		return employee
	}

	return employer
}
