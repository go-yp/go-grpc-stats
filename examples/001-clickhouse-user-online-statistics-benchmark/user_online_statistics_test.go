package main

import (
	"database/sql"
	_ "github.com/ClickHouse/clickhouse-go"
	"sync/atomic"
	"testing"
	"time"
)

func BenchmarkUserOnlineStatisticsStore(b *testing.B) {
	const (
		packSize = 10000
	)

	var startTime = time.Now()

	connect, err := sql.Open("clickhouse", "tcp://clickhouse:9000?database=yaaws")
	if err != nil {
		b.Fatal(err)
	}

	if err := connect.Ping(); err != nil {
		b.Fatal(err)
	}

	if err := resetTable(connect); err != nil {
		b.Fatal(err)
	}

	var fixtures = userOnlineEventFixtures(packSize)

	var count uint32 = 0

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var storeErr = StoreUserOnlineStatistics(connect, fixtures)
			if storeErr != nil {
				b.Error(storeErr)

				continue
			}

			atomic.AddUint32(&count, 1)
		}
	})

	b.Logf("inserted count %8d by %5d %s", count*packSize, count, time.Since(startTime))
}
