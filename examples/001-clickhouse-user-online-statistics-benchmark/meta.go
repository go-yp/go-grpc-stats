package main

import "database/sql"

const (
	// language=SQL
	createTableSQL = `CREATE TABLE user_online_source_statistics
(
    date    Date,
    time    DateTime,
    user_id UInt32,
    role    UInt8
) ENGINE = MergeTree()
      PARTITION BY date
      ORDER BY user_id;`

	// language=SQL
	dropTableSQL = `DROP TABLE IF EXISTS user_online_source_statistics;`
)

func resetTable(connect *sql.DB) error {
	if _, err := connect.Exec(dropTableSQL); err != nil {
		return err
	}

	if _, err := connect.Exec(createTableSQL); err != nil {
		return err
	}

	return nil
}
