package main

import (
	"context"
	"gitlab.com/go-yp/go-grpc-stats/models/protos/services/stats"
	"google.golang.org/grpc"
	"log"
	"runtime"
	"sync"
	"time"
)

// 2021/06/27 18:01:00 complete 9600000 messages by 1m35.037260474s
func main() {
	const messageCount = 100000

	var startTime = time.Now()

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("[CRITICAL] failed to connect: %w\n", err)
	}
	defer conn.Close()

	client := stats.NewStatsClient(conn)

	var (
		wg       = new(sync.WaitGroup)
		numProcs = 8 * runtime.GOMAXPROCS(0)
	)

	for i := 0; i < numProcs; i++ {
		wg.Add(1)

		go func(i int) {
			defer wg.Done()

			for j := 0; j < messageCount; j++ {
				if j%1000 == 0 {
					log.Printf("[%3d] %6d messages by %s", i, j, time.Since(startTime))
				}

				response, err := client.StoreOnlineEvent(context.Background(), &stats.OnlineEvent{
					Time:        int32(j),
					UserId:      uint32(j),
					Role:        uint32(j%2) + 1,
					CountryCode: "UA",
					City:        "Kyiv",
				})

				if err != nil {
					log.Printf("[CRITICAL] failed to connect: %v\n", err)
				}

				_ = response
			}
		}(i)
	}

	wg.Wait()

	log.Printf("complete %d messages by %s", numProcs*messageCount, time.Since(startTime))
}
