package main

import (
	"context"
	"database/sql"
	"gitlab.com/go-yp/go-grpc-stats/models/protos/services/stats"
	"google.golang.org/grpc"
	"log"
	"net"
)

type Server struct {
	connect *sql.DB
}

func NewServer(connect *sql.DB) *Server {
	return &Server{connect: connect}
}

func (s *Server) StoreOnlineEvent(ctx context.Context, event *stats.OnlineEvent) (*stats.EmptyResponse, error) {
	// @TODO логіка збереження в буфер
	_ = event

	return &stats.EmptyResponse{}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("[CRITICAL] failed to listen: %v", err)
	}
	defer lis.Close()

	var (
		connect     *sql.DB           = nil
		statsServer stats.StatsServer = NewServer(connect)
	)

	s := grpc.NewServer()

	stats.RegisterStatsServer(s, statsServer)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("[CRITICAL] Server exited with err: %v", err)
	}
}
