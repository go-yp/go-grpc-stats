module gitlab.com/go-yp/go-grpc-stats

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.4.5
	github.com/golang/protobuf v1.5.2
	google.golang.org/grpc v1.39.0
)
